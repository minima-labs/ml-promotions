<?php
/**
 * @file
 * ml_promotion_groups.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ml_promotion_groups_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_promotion_refs'
  $field_bases['field_promotion_refs'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_promotion_refs',
    'foreign keys' => array(
      'promotion' => array(
        'columns' => array(
          'target_id' => 'pid',
        ),
        'table' => 'promotion',
      ),
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'direction' => 'ASC',
          'property' => 'title',
          'type' => 'property',
        ),
        'target_bundles' => array(),
      ),
      'target_type' => 'promotion',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );

  return $field_bases;
}
