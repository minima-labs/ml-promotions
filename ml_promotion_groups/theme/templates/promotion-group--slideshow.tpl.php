<div<?php print $attributes; ?>>
  <div class="promotion-group__progress"><div class="inner"></div></div>

  <?php if ($show_controls): ?>
  <div class="promotion-group__controls">
    <div class="promotion-group__prev icon--angle-left"></div>
    <div class="promotion-group__pager">
      <?php foreach ($promotions as $promotion): ?>
        <span class="icon--circle"></span>
      <?php endforeach; ?>
    </div>
    <div class="promotion-group__next icon--angle-right"></div>
  </div>
  <?php endif; ?>

  <?php foreach ($promotions as $promotion): ?>
    <?php print render($promotion); ?>
  <?php endforeach; ?>
</div>
