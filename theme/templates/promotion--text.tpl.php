<div<?php print $attributes; ?>>
  <?php if ($link): ?><a<?php print $link_attributes; ?>><?php endif; ?>
    <h2 class="promotion__title"><?php print $title; ?></h2>
    <div class="promotion__body"><?php print $body; ?></div>
    <?php if ($cta): ?>
      <div class="promotion__cta button"><?php print $cta; ?></div>
    <?php endif; ?>
  <?php if ($link): ?></a><?php endif; ?>
</div>
